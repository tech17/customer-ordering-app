package com.example.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.customerapp.Adapter.ItemsAdapter;
import com.example.customerapp.Models.Item;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    private RecyclerView ordersRV;
    ItemsAdapter adapter;

    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton newGrpBtn;

    private Dialog UserDialog;
    private ImageView CloseBillDialog;
    private Button orderBtn;
    private EditText name, qty;

    public static List<Item> orders = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        ordersRV = findViewById(R.id.ordersRV);
        ordersRV.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ordersRV.smoothScrollToPosition(0);

        populateSampleItems();

        adapter = new ItemsAdapter(getApplicationContext(), orders);
        ordersRV.setAdapter(adapter);

        floatingActionMenu = findViewById(R.id.fab);
        floatingActionMenu.setClosedOnTouchOutside(true);

        newGrpBtn = findViewById(R.id.newGrpBtn);
        newGrpBtn.setOnClickListener(v -> {
            floatingActionMenu.close(true);
            UserDialog.show();

        });

        UserDialog = new Dialog(this);
        UserDialog.setCanceledOnTouchOutside(false);
        UserDialog.setContentView(R.layout.order_dialog);
        UserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CloseBillDialog = UserDialog.findViewById(R.id.CloseBillDialog);
        orderBtn = UserDialog.findViewById(R.id.orderBtn);
        qty = UserDialog.findViewById(R.id.qty);
        name = UserDialog.findViewById(R.id.name);
        orderBtn = UserDialog.findViewById(R.id.orderBtn);

        CloseBillDialog.setOnClickListener((view) -> {
            UserDialog.dismiss();
        });
        orderBtn.setOnClickListener((view) -> {
            SweetAlertDialog s = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Successful!").setConfirmClickListener(
                            sweetAlertDialog -> {
                                sweetAlertDialog.dismiss();
                                name.setText("");
                                qty.setText("");
                                UserDialog.dismiss();

                            }
                    )
                    .setContentText("Order placed successfully!");


            s.show();

        });



    }

    private void populateSampleItems() {
//String name, String customerName, String cost, String status
        Item item1 = new Item("Item 1", "Ones Muchoki", "23,334", "Delivered", R.drawable.item1);
        Item item2 = new Item("Item 2", "Ann Muli", "50,331", "Delivered", R.drawable.item2);
        Item item3 = new Item("Item 3", "Ones Muchoki", "23,000", "Cancelled", R.drawable.item3);
        Item item4 = new Item("Item 4", "James Lai", "8,432", "Ongoing", R.drawable.item4);
        Item item5 = new Item("Item 5", "Ruth Otis", "10,456", "Ongoing", R.drawable.item5);

        orders.add(item1);
        orders.add(item2);
        orders.add(item3);
        orders.add(item4);
        orders.add(item5);
    }
}