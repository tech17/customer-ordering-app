package com.example.customerapp.Models;

public class Item {
    String name;
    String customerName;
    String cost;
    String status;
    int imgId;

    public Item() {
    }

    public Item(String name, String customerName, String cost, String status, int imgId) {
        this.name = name;
        this.customerName = customerName;
        this.cost = cost;
        this.status = status;
        this.imgId = imgId;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
