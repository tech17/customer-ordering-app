package com.example.customerapp.Models;

public class Customer {
    private String name;
    private String email;
    private String phone;
    private String idNo;
    private String gender;
    private String agent;


    public Customer() {
    }

    public Customer(String name, String email, String phone, String idNo, String gender, String agent) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.idNo = idNo;
        this.gender = gender;
        this.agent = agent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
}
