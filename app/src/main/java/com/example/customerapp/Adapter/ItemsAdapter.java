package com.example.customerapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.customerapp.Models.Item;
import com.example.customerapp.R;

import java.util.Collections;
import java.util.List;


/**
 * Created by Mulu Kadan 26/09/2021.
 */

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.myViewHolder> {


    private LayoutInflater inflator;
    List<Item> data = Collections.emptyList();
    Context mContext;

    public ItemsAdapter(Context context, List<Item> data) {
        inflator= LayoutInflater.from(context);
        mContext = context;
        this.data = data;
        //Fetching Notifications
//        this.data = db.getAllNotifications();
        Collections.reverse(data);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.item_view,parent,false);
        myViewHolder holder = new myViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
       try {
           Item current = data.get(position);
           holder.title.setText(current.getName());

           holder.Time.setText(current.getStatus());
//           String ltr = current.getName().substring(0,1).toUpperCase();
//           holder.Icon.setText(ltr);
           holder.Icon.setVisibility(View.GONE);
           holder.agent.setText("Ksh. "+current.getCost());

           holder.title.setTextColor(Color.parseColor("#00B1A8"));

          holder.iconBK.setImageResource(current.getImgId());

       }catch (Exception e){

       }
    }

    public String GetColour(String Letter){
        String Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int pos = Alphabet.indexOf(Letter) + 1;
        String Clr ="";

        if(pos%2 == 0){
            Clr ="#7381CA";
        }else if(pos%3 == 0){
            Clr ="#4FC2F7";
        }else if (pos%5 == 0){
            Clr ="#AED581";
        }else{
            Clr ="#4CB6AC";
        }
        return Clr;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder{

        TextView Time;
        TextView agent;
        TextView title;
        TextView Icon;
        ImageView iconBK;


        public myViewHolder(final View itemView) {
            super(itemView);
            Time = itemView.findViewById(R.id.time);
            agent = itemView.findViewById(R.id.agent);
            title = itemView.findViewById(R.id.title);
            Icon = itemView.findViewById(R.id.icon);
            iconBK = itemView.findViewById(R.id.iconBK);

            itemView.setOnClickListener(v -> {
                int pos = getAdapterPosition();
                if(pos != RecyclerView.NO_POSITION){
                    Item item = data.get(pos);
                }
//                    Toast.makeText(itemView.getContext(), "", Toast.LENGTH_SHORT).show();
            });
        }
    }
}
